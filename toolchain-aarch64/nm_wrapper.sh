#!/bin/bash
set -euo pipefail

FDSDK="external/fdsdk"
ARCH=aarch64

LD_LIBRARY_PATH=$FDSDK/usr/lib/sdk/toolchain-$ARCH/x86_64-unknown-linux-gnu/$ARCH-unknown-linux-gnu/lib
export LD_LIBRARY_PATH

exec $FDSDK/usr/lib/sdk/toolchain-$ARCH/bin/$ARCH-unknown-linux-gnu-gcc-nm "$@"
