Freedesktop SDK toolchains for Bazel
=================

# Running the hello-world demo

* Install BuildStream and Buildstream External
* Build the toolchain:

```
    $ bst build toolchain-tarball-x86.bst
```

* Checkout the toolchain tarball

```
    $ bst checkout toolchain-tarball-x86_64.bst toolchain-tarball-x86_64
```

* The current version of Freedesktop SDK used builds with libc 2.27. If your
  system's libc is older than this you won't be able to build with Bazel. You
  can however create a more up-to-date chroot or container and use Bazel from
  within that, copying in the checked out tarball.

* Edit the WORKSPACE file in 'tests' and change the file:/// url to point to the tarball:

```
    load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive")

    http_archive(
        name = "fdsdk",
        urls = [
            "file:///home/rdale/build/fdsdk_bazel/bazel-toolchains-fdsdk/toolchain-tarball-x86_64/freedesktop-sdk-toolchain-x86_64.tar.xz",
        ],
    )
```

* Add these lines to your ~/.bazelrc file

```
    build:fdsdk --crosstool_top=@fdsdk//toolchain:fdsdk
    build:fdsdk --cpu=x86_64
```

* Build the hello-world target in 'tests':

```
    $ bazel build --config=fdsdk //main:hello-world
```

* Run the hello-world executable:

```
    $ bazel run --config=fdsdk //main:hello-world
```

# Building the hello-world demo for an aarch64 target

* Build the toolchain:

```
    $ bst build toolchain-tarball-aarch64.bst
```

* Checkout the toolchain tarball

```
    $ bst checkout toolchain-tarball-aarch64.bst toolchain-tarball-aarch64
```

* Edit the WORKSPACE file in 'tests' and change the file:/// url to point to the tarball:

```
    load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive")

    http_archive(
        name = "fdsdk",
        urls = [
            "file:///home/rdale/build/fdsdk_bazel/bazel-toolchains-fdsdk/toolchain-tarball-aarch64/freedesktop-sdk-toolchain-x86_64-aarch64.tar.xz",
        ],
    )
```

* Build the hello-world executable

```
    $ bazel build --crosstool_top=@fdsdk//toolchain:fdsdk --cpu=aarch64 //main:hello-world
```
